/**
 * Escapes the given HTML string.
 *
 * @param {string} str
 *
 * @return {string}
 */
function escapeHTML(str) {
    const p = document.createElement("p");
    p.appendChild(document.createTextNode(str));
    return p.innerHTML;
}

/**
 * Returns the type of a value, or its class name if it is an object.
 *
 * @param {*} v
 *
 * @return {string}
 */
function getType(v) {
    const type = typeof v;
    if (type === 'object') {
        return v.constructor.name;
    } else {
        return type;
    }
}

/**
 * Debounce pattern.
 *
 * @param {function} func
 * @param {number}   delay
 *
 * @return {function}
 */
const debounce = (func, delay) => {
    let inDebounce

    return function() {
        const context = this;
        const args = arguments;
        clearTimeout(inDebounce);
        inDebounce = setTimeout(() => func.apply(context, args), delay);
    };
}

/**
 * Builds a URL encoded body suitable for using with fetch().
 *
 * @param {object} data  A key/value list of variables
 *
 * @return {URLSearchParams}
 */
function urlEncodedBody(data) {
    const body = new URLSearchParams();
    for (const key in data) {
        if (data.hasOwnProperty(key)) {
            let value = data[key];

            if (typeof value === 'boolean') {
                value = value ? 'true' : 'false';
            }

            if (typeof value !== 'string') {
                continue;
            }

            body.append(key, value);
        }
    }

    return body;
}

module.exports = {
    escapeHTML,
    getType,
    debounce,
    urlEncodedBody,
};
