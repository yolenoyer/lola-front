import '../../../css/pages/session.scss';

const $ = require('jquery');

require('./session_command');

const modal = require('../../include/modal');
const { Modal } = modal;
const session_handler = require('./session_handler');
const {
    deleteCurrentSession,
    resetCurrentSession,
    renameCurrentSession
} = require('./session_edit');
const lolaApi = require('../../lolaApi');
const { SessionNameTextInput } = require('../../include/session_name_text_input');

const SESSION_ID = app.session.session_id;

function onLoad() {
    const sessionHandler = session_handler.start();

    modal.setupActionAndButton(
        'ok_no_more_warning',
        {
            key: 'o',
        },
        {
            title: `
                <i class="fas fa-check text--success"></i>
                Ok
                <span class="text--small">(ne plus m'avertir)</span>
            `,
        }
    );

    $('.dropdown-menu__menu .command').click(() => {
        closeDropdownMenu();
    });

    $('.delete-session--btn').click(() => {
        modal.show('modal--confirm-delete-session', {
            confirm: deleteCurrentSession,
        });
    });

    $('.reset-session--btn').click(() => {
        modal.show('modal--confirm-reset-session', {
            confirm: resetCurrentSession,
        });
    });

    $('.rename-session--btn').click(() => {
        const renameModal = modal.show('modal--rename-session', {
            confirm: modal => {
                const newName = $(modal.dom).find('.modal--rename-session__new-name').val();
                renameCurrentSession(newName);
            }
        });

        setupRenameModal(renameModal);
    });

    $('.export-session--btn').click(() => {
        sessionHandler.export();
    });

    const $accessibilityCheckbox = $('input[type=checkbox][name=is_public]');
    $accessibilityCheckbox.change(() => {
        const isPublic = $accessibilityCheckbox.prop('checked');
        lolaApi.setSessionAccessibility(SESSION_ID, isPublic).then(() => {});
    });
}

/**
 * Close the dropdown menu, after having clicked on an item.
 */
function closeDropdownMenu() {
    const menu = $('.dropdown-menu__menu');
    menu.hide();
    setTimeout(() => menu.show(), 100);
}

/**
 * Setups the rename session modal.
 *
 * @param {Modal} renameModal
 */
function setupRenameModal(renameModal) {
    const $input = $(renameModal.contentBody).find('.modal--rename-session__new-name');

    new SessionNameTextInput({
        input: $input,
        messageSpan: '.with-notice-area__area',
        initialName: SESSION_ID,
        invalidMessage: session => {
            if (session.name() === '' || session.name() === SESSION_ID) {
                return '';
            } else {
                return "Nom déjà existant";
            }
        },
        onStateChange: sessionInput => {
            $(renameModal.contentBody).toggleClass('error', sessionInput.state.isInvalid());
            renameModal.toggleAllowAction('confirm', sessionInput.state.isValid());
            if (sessionInput.enterWasPressed && sessionInput.state.isValid()) {
                sessionInput.confirm();
                renameModal.triggerAction('confirm');
            }
        },
    });
}

$(onLoad);
