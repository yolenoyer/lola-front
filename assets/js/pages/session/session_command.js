const $ = require('jquery');
const { CodeJar } = require('codejar');

const lolaApi = require('../../lolaApi');
const { escapeHTML } = require('../../util');
const { getTemplate } = require('../../include/templating');
const modal = require('../../include/modal');
const { showError } = require('./error');
const { highlight, highlighter } = require('../../code_highlighting');

const SESSION_ID = app.session.session_id;

/**
 * DOM handler for one session command.
 */
class SessionCommand {
    /**
     * Contructor.
     *
     * @param {SessionHandler}   sessionHandler
     * @param {object?} data           Session command data, returned by the Lola api, or
     *                                          keep undefined to create empty live command block
     */
    constructor(sessionHandler, data) {
        this.sessionHandler = sessionHandler;

        this.$dom = $(getTemplate('template--session-command'));
        this.$dom.data('session-command', this)

        this.$input = this.$dom.find('.input-area');
        this.$output = this.$dom.find('.output-area');
        this.$toggleEditModeButton = this.$dom.find('.toggle-input-style--btn');

        this.isInserted = false;
        this.beforeCommandId = null;

        this.setupButtons();
        this.setupKeyboard();
        this.setupFromData(data);
    }

    /**
     * Setups the command buttonContainers on startup.
     */
    setupButtons() {
        this.$dom.on('click', '.toggle-input-style--btn', () => {
            this.toggleInputStyle();
        });

        this.$dom.on('click', '.import-data--btn', () => {
            const importModal = modal.show('modal--import-session', {
                confirm: () => {
                    let data;
                    try {
                        data = JSON.parse($dataTextarea.val());
                    }
                    catch {}
                    if (typeof data !== 'object' || !Array.isArray(data.history)) {
                        showImportError();
                    } else {
                        this.runImported(data.history);
                    }
                },
            });
            const $dataTextarea = $(importModal.dom).find('.json--data');
            $dataTextarea
                .click(ev => ev.stopPropagation())
                .keydown(ev => {
                    if (ev.originalEvent.key !== 'Escape') {
                        ev.stopPropagation();
                    }
                })
                .focus();
        })

        this.$dom.on('click', '.delete-session-command--btn', ev => {
            if (ev.originalEvent.ctrlKey) {
                this.delete();
            } else if (this.sessionHandler.changeHistoryWarning) {
                this.sessionHandler.confirmChangeHistory(
                    'modal--confirm-delete-session-command',
                    () => this.delete()
                );
            } else {
                modal.show('modal--confirm-delete-session-command--no-warn', {
                    confirm: () => this.delete(),
                });
            }
        })

        this.$dom.on('click', '.edit-session-command--btn', () => {
            this.toggleEditable();
        });

        this.$dom.on('click', '.insert-session-command--btn', () => {
            this.sessionHandler.insertLiveCommandBefore(this);
        });

        this.$dom.on('click', '.input-area', ev => {
            if (ev.originalEvent.ctrlKey) {
                this.toggleEditable();
            }
        });
    }

    /**
     * Setups the keyboard: validation with Enter / Ctrl-Enter, arrow keys...
     */
    setupKeyboard() {
        this.$input.keydown(ev => {
            let handled = false;

            const key = ev.originalEvent.key;
            const ctrlKey = ev.originalEvent.ctrlKey;
            const shiftKey = ev.originalEvent.shiftKey;
            const singleLine = this.inputStyle === 'single';
            const multiLine = !singleLine;

            switch (key) {
                case 'Enter':
                    // <Enter> in single-line style, or <Ctrl+Enter> in multi-line style:
                    if (singleLine && !shiftKey || multiLine && ctrlKey) {
                        if (this.isInserted) {
                            this.sessionHandler.confirmChangeHistory(
                                'modal--confirm-insert-session-command',
                                () => this.run()
                            );
                        } else if (this.isCompleted) {
                            this.sessionHandler.confirmChangeHistory(
                                'modal--confirm-edit-session-command',
                                () => this.run()
                            );
                        } else {
                            this.run();
                        }
                        handled = true;
                    }
                    break;
                case 'Escape':
                    if (this.isCompleted) {
                        this.setEditable(false);
                    } else if (this.isInserted) {
                        this.$dom.remove();
                    }
                    handled = true;
            }

            // In single-line mode, arrow keys <Up> and <Down> will:
            //  - Browse history when no <Ctrl> key is pressed;
            //  - Move the cursor when <Ctrl> key is pressed;
            // In multi-line mode, arrow keys <Up> and <Down> will:
            //  - Move the cursor when no <Ctrl> key is pressed;
            //  - Browse history when <Ctrl> key is pressed;
            const escapedArrowKey = (multiLine || ctrlKey) && (singleLine || !ctrlKey);

            switch (key) {
                case 'ArrowUp':
                    // <Up> in single-line style, or <Ctrl+Up> in multi-line style:
                    if (!escapedArrowKey) {
                        this.sessionHandler.historyBack();
                        handled = true;
                    }
                    break;
                case 'ArrowDown':
                    // <Down> in single-line style, or <Ctrl+Down> in multi-line style:
                    if (!escapedArrowKey) {
                        this.sessionHandler.historyForward();
                        handled = true;
                    }
                    break;
                case 'ArrowLeft':
                case 'ArrowRight':
                    break;
                default: {
                    this.sessionHandler.historyIndex = -1;
                    break;
                }
            }

            ev.stopPropagation();

            if (handled) {
                return false;
            }
        })
    }

    /**
     * Setups the command accordingly to the given data:
     *  - if there is some data (coming from the "run code" api request), then the command is setup
     *    as a history command (output is visible, and input/output dom element are updated);
     *  - if there is no data, then the command is setup as a live empty command.
     *
     * @param {object?} data
     */
    setupFromData(data) {
        const hasData = typeof data === 'object';

        if (hasData) {
            // History commmand:
            this.data = data;
            this.setCompleted(true);
            this.setInputStyle(this.detectBestInputStyle());
            this.setEditable(false);
            this.commandId = data.command_id;
            this.setInput(data.input);
            this.setOutput(data);
        } else {
            // Live command:
            this.data = null;
            this.setCompleted(false);
            this.setInputStyle(this.detectBestInputStyle());
            this.setEditable(true);
            this.commandId = null;
        }
    }

    /**
     * After a run response, sets the output area of the command.
     *
     * @param {object} data Command information
     */
    setOutput(data) {
        let result;
        if (data.result.ok !== undefined) {
            result = highlighter.highlight(data.result.ok);
            result = `<div class="result-success">${result}</div>`;
        } else {
            result = escapeHTML(data.result.error);
            result = `<div class="result-error">${result}</div>`;
        }

        const output = escapeHTML(data.output.trim());

        let whole_output = '';
        if (output.length > 0) {
            whole_output += `<span class="output">${output}</span>\n`;
        }
        whole_output += result;

        this.$output.html(whole_output.trim());
    }

    /**
     * Returns the actual input content.
     *
     * @return {string}
     */
    getInput() {
        const lines = [];
        const $divs = this.$input.children('div');
        if ($divs.length > 0) {
            $divs.each((_, div) => {
                lines.push(div.innerText);
            });
        } else {
            lines.push(this.$input.text());
        }
        return lines.join('\n');
    }

    /**
     * Sets the input content.
     *
     * @param {string} text
     */
    setInput(text) {
        this.jar.updateCode(text);
    }

    /**
     * Detects if the input has to be setup as multi-line.
     *
     * @return {string}
     */
    detectBestInputStyle() {
        if (this.isCompleted && this.data.input.match('\n')) {
            return 'multi';
        } else {
            return 'single';
        }
    }

    /**
     * Toggles the style of the command input: single-line or multi-line.
     */
    toggleInputStyle() {
        if (this.inputStyle === 'single') {
            this.setInputStyle('multi');
        } else {
            this.setInputStyle('single');
        }
    }

    /**
     * Sets the style of the command input: single-line or multi-line.
     *
     * @param {string} newStyle Possible values: 'single', 'multi'
     */
    setInputStyle(newStyle) {
        this.inputStyle = newStyle;

        let text;
        if (this.inputStyle === 'single') {
            text = 'Multiligne';
        } else {
            text = 'Ligne simple';
        }
        this.$toggleEditModeButton.text(text);

        let $newInput = $('<div class="input-area">');
        const newValue = this.isCompleted ? this.data.input : this.getInput();

        this.$input.after($newInput);
        this.$input.remove();
        this.$input = $newInput;
        this.setupKeyboard();
        // noinspection JSValidateTypes
        this.jar = CodeJar(this.$input[0], highlight, {
            tab: ' '.repeat(4),
        });
        this.setInput(newValue);
        this.$input.focus();

        // Disable the input or not, accordingly to `this.isEditable`:
        this.updateEditable();
    }

    /**
     * Defines if the command is completed or not (change appearance accordingly).
     *
     * @param {boolean} value
     */
    setCompleted(value = true) {
        this.isCompleted = value;
        this.$dom.toggleClass('session-command--completed', value);
    }

    /**
     * Toggles the editable mode.
     */
    toggleEditable() {
        this.isEditable = !this.isEditable;
        this.updateEditable();
    }

    /**
     * Sets (or unset) the editable mode of the command.
     *
     * @param {boolean} value
     */
    setEditable(value = true) {
        this.isEditable = value;
        this.updateEditable();
    }

    /**
     * Update the editable mode of the command, accordingly to `this.isEditable`.
     */
    updateEditable() {
        this.$dom.toggleClass('session-command--editable', this.isEditable);
        this.$input.blur();
        this.$input.prop('contenteditable', this.isEditable);
        if (!this.isEditable && this.isCompleted) {
            this.setInput(this.data.input);
        }
        if (this.isEditable) {
            this.$input.focus();
        }
    }

    /**
     * Sets the command as an "inserted" command (the command is empty and has been inserted into
     * the history, before the given command id).
     *
     * @param {string} beforeCommandId
     */
    setInsertedBefore(beforeCommandId) {
        this.isInserted = true;
        this.beforeCommandId = beforeCommandId;
    }

    /**
     * Runs the actual code into the lola session, and archive the command into the history zone on
     * success.
     */
    run() {
        const code = this.getInput();

        if (this.isInserted) {
            lolaApi.insertCommand(SESSION_ID, this.beforeCommandId, code)
            .then(res => {
                if (res.success) {
                    app.session = res.payload.session_information;
                    this.sessionHandler.reloadHistory();
                } else {
                    showError(res.error);
                }
            });
        } else if (this.isCompleted) {
            lolaApi.editCommand(SESSION_ID, this.commandId, code)
            .then(res => {
                if (res.success) {
                    app.session = res.payload.session_information;
                    this.sessionHandler.reloadHistory();
                } else {
                    showError(res.error);
                }
            });
        } else {
            lolaApi.runCode(SESSION_ID, code)
            .then(res => {
                if (res.success) {
                    this.archive(res.payload);
                } else {
                    showError(res.error);
                }
            });
        }
    }

    /**
     * Run multiple commands at once.
     *
     * @param {string[]} data
     */
    runImported(data) {
        if (this.isInserted) {
            lolaApi.insertMultipleCommands(SESSION_ID, this.beforeCommandId, data)
            .then(res => {
                if (res.success) {
                    app.session = res.payload.session_information;
                    this.sessionHandler.reloadHistory();
                } else {
                    showError(res.error);
                }
            })
            .catch(_ => {
                showImportError();
            });
        } else if (this.isCompleted) {
            console.error('Import is not available on an existing command');
        } else {
            lolaApi.runMultipleCode(SESSION_ID, data)
            .then(res => {
                if (res.success) {
                    this.archive(res.payload);
                } else {
                    showError(res.error);
                }
            })
            .catch(_ => {
                showImportError();
            });
        }
    }

    /**
     * Put this command into the history zone, and add new live command.
     *
     * @param {object} data The response of the run api call
     */
    archive(data) {
        this.$dom.remove();

        for (const command_data of data.commands) {
            const sessionCommand = new SessionCommand(this.sessionHandler, command_data);
            this.sessionHandler.pushToHistory(sessionCommand);
            app.session.commands.push(command_data);
        }

        const command = this.sessionHandler.newLiveCommand();
        if (this.inputStyle === 'multi') {
            command.toggleInputStyle();
        }
    }

    /**
     * Deletes this command: fetches the request to the api, and remove the dom.
     */
    delete() {
        lolaApi.deleteCommand(SESSION_ID, this.commandId)
        .then(res => {
            if (res.success) {
                app.session = res.payload.session_information;
                this.sessionHandler.reloadHistory();
            } else {
                alert('Une erreur est survenue.');
            }
        })
    }
}

/**
 * Displays an error when data import has failed.
 */
function showImportError() {
    showError("Impossible d'importer les données: format non-reconnu");
}

module.exports = {
    SessionCommand,
}
