const lolaApi = require('../../lolaApi');
const { route } = require('../../include/routing');

const SESSION_ID = app.session.session_id;

/**
 * Deletes the current session and go back to the home page.
 */
function deleteCurrentSession() {
    lolaApi.deleteSession(SESSION_ID)
    .then(() => {
        window.location = route('home');
    })
}

/**
 * Resets the current session and reload.
 */
function resetCurrentSession() {
    lolaApi.resetSession(SESSION_ID)
    .then(() => {
        window.location.reload();
    })
}

/**
 * Renames the current session and redirects to the new url.
 *
 * @param {string} newName
 */
function renameCurrentSession(newName) {
    lolaApi.renameSession(SESSION_ID, newName)
    .then(() => {
        window.location = route('session', { sessionId: newName });
    });
}

module.exports = {
    deleteCurrentSession,
    resetCurrentSession,
    renameCurrentSession,
}
