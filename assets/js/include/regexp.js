let { getType } = require('../util');

/**
 * Returns a regex string, without the flags and the slash separators.
 *
 * @param {RegExp|string} regex
 *
 * @return {string}
 */
function toString(regex) {
    switch (getType(regex)) {
        case 'string': return regex;
        case 'RegExp': return regex.source;
        default: throw 'Type error';
    }
}

/**
 * Creates a new regular expression by replace `%1`, '%2', etc... by the given regex arguments.
 *
 * @param {RegExp} regex
 * @param {RegExp|string} args
 *
 * @return {RegExp}
 */
function regexf(regex, ...args) {
    let regex_str = toString(regex);
    regex_str = regex_str.replace(/%(\d+)/g, (_, n) => {
        n = parseInt(n);
        return toString(args[n-1]);
    });

    return new RegExp(regex_str, regex.flags);
}

/**
 * Creates a regex which will match a list of words.
 *
 * @param {string[]} words
 *
 * @return {RegExp}
 */
function fromWordList(words) {
    return regexf(/(?:\b(?:%1)\b)/, words.join('|'));
}

/**
 * Creates a regex which will match any of the given regexes.
 *
 * @param {[RegExp|string]} regexes
 * @param {string}          [flags='']
 * @param {boolean}         capture
 *
 * @return {RegExp}
 */
function any(regexes, flags = '', capture = false) {
    const format = capture ? /(%1)/ : /(?:%1)/;
    const strings = regexes.map(regex => regexf(format, regex).source);

    return new RegExp(strings.join('|'), flags);
}

/**
 * Checks if the browser supports lookbehind regex expressions.
 *
 * @return {boolean}
 */
function checkLookBehindSupport() {
    try {
        new RegExp('(?<=test)');
        return true;
    }
    catch {
        return false;
    }
}

module.exports = {
    toString,
    fromWordList,
    regexf,
    any,
    checkLookBehindSupport,
}
