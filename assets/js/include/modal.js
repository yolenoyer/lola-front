const $ = require('jquery');

// Default buttonContainers:
const BUTTONS = {
    yes: {
        title: '<i class="fas fa-check text--success"></i> Oui',
        action: 'confirm',
    },
    ok: {
        title: '<i class="fas fa-check text--success"></i> Ok',
        action: 'confirm',
    },
    no: {
        title: '<i class="fas fa-times text--danger"></i> Non',
        action: 'cancel',
    },
    cancel: {
        title: '<i class="fas fa-times text--danger"></i> Annuler',
        action: 'cancel',
    },
}

// Default actions:
const ACTIONS = {
    confirm: {
        key: 'Enter',
        keyTitle: 'Entrée',
    },
    cancel: {
        key: 'Escape',
        keyTitle: 'Échap.',
    },
};

let modalTemplate;
document.addEventListener('DOMContentLoaded', function() {
    modalTemplate = document.querySelector('#modal-template');
}, false);

class Modal {
    /**
     * Constructor.
     *
     * @param {string} contentTemplateId
     * @param {object} [actions] Each key must be a key from the `BUTTONS` dict, except the special key
     *                           `init` which can be a function which will be called once the dom is
     *                           ready
     */
    constructor(contentTemplateId, actions) {
        this.actions = actions || {};

        this.dom = document.importNode(modalTemplate.content, true).children[0];
        this.modalBackground = this.dom.querySelector('.modal__background');
        this.modalBody = this.dom.querySelector('.modal__body');
        this.modalLogoIcon = this.dom.querySelector('.modal__logo-icon');
        this.modalCommands = this.dom.querySelector('.modal__commands');

        this.contentTemplate = document.querySelector(`#${contentTemplateId}`);
        this.dom.classList.add(this.contentTemplate.id);
        this.content = document.importNode(this.contentTemplate.content, true);

        // Body:
        this.contentBody = this.content.querySelector('.modal__body');
        this.modalBody.replaceWith(this.contentBody);

        // Logo icon:
        this.contentLogoIcon = this.content.querySelector('.modal__logo-icon');
        this.modalLogoIcon.replaceWith(this.contentLogoIcon);

        // Command buttons (wrapped into <div> button containers):
        const modalButtons = this.contentTemplate.getAttribute('data-modal-buttons');
        this.buttonNames = modalButtons.split(/\s*,\s*/);
        this.buttonContainers = this.buttonNames.map(name => createButtonContainer(name));

        for (const buttonContainer of this.buttonContainers) {
            this.modalCommands.appendChild(buttonContainer);
            $(buttonContainer).find('button').on('click', ev => {
                const $this = $(ev.currentTarget);
                const action = $this.parents('.modal__button-container')
                    .first()
                    .data('modal-action');
                this.triggerAction(action);
                ev.stopPropagation();
            });
        }

        // Background:
        $(this.modalBackground).on('click', () => {
            this.triggerAction('cancel');
        })

        this.forbiddenActions = new Set();
    }

    /**
     * Find an action which could be triggered by the given keyboard event.
     *
     * @param {KeyboardEvent} ev
     *
     * @return {string|undefined}
     */
    findActionByKey(ev) {
        for (let action in ACTIONS) {
            const actionDef = ACTIONS[action];
            if (actionDef.key !== undefined && actionDef.key === ev.key) {
                return action;
            }
        }
        return undefined;
    }

    /**
     * Triggers the given action and closes the modal.
     *
     * @param {string} action
     */
    triggerAction(action) {
        if (!this.actionIsAllowed(action)) {
            return;
        }

        const actionHandler = this.actions[action];
        if (typeof actionHandler === 'function') {
            actionHandler(this, action);
        }

        const actionDef = ACTIONS[action];
        const mustClose = actionDef?.close ?? true;

        if (mustClose) {
            this.close();
        }
    }

    /**
     * (Re)allows the given action.
     *
     * @param {string} action
     */
    allowAction(action) {
        this.forbiddenActions.delete(action);
        this.enableOrDisableButtons(action);
    }

    /**
     * Forbids the given action.
     *
     * @param {string} action
     */
    forbidAction(action) {
        this.forbiddenActions.add(action);
        this.enableOrDisableButtons(action);
    }

    /**
     * Allows or forbids the given action.
     *
     * @param {string}  action
     * @param {boolean} toggleValue
     */
    toggleAllowAction(action, toggleValue) {
        if (toggleValue) {
            this.allowAction(action);
        } else {
            this.forbidAction(action);
        }
    }

    /**
     * Returns true if the given action is allowed.
     *
     * @param {string} action
     *
     * @return {boolean}
     */
    actionIsAllowed(action) {
        return !this.forbiddenActions.has(action);
    }

    /**
     * Gets all buttons (containers) which can trigger the given actions.
     *
     * @param {string} action
     *
     * @return {HTMLElement[]}
     */
    getButtonContainersTriggering(action) {
        return this.buttonContainers.filter(button => {
            return button.getAttribute('data-modal-action') === action;
        });
    }

    /**
     *
     * @param {string} action
     */
    enableOrDisableButtons(action) {
        $(this.getButtonContainersTriggering(action))
            .find('button')
            .prop('disabled', !this.actionIsAllowed(action));
    }

    /**
     * Shows the dom.
     */
    open() {
        document.body.appendChild(this.dom);
        $(':focus').blur();
        $('body').on('keydown.modal', ev => {
            ev = ev.originalEvent;
            const action = this.findActionByKey(ev);
            if (action !== undefined) {
                this.triggerAction(action);
            }
        });
    }

    /**
     * Closes the dom.
     */
    close() {
        this.dom.remove();
        $('body').off('.modal');
    }
}

/**
 * Creates a new button accordingly to the `BUTTON` dict.
 *
 * @param {string} name Name contained in the `BUTTON` dict
 *
 * @return {HTMLElement}
 */
function createButtonContainer(name) {
    const def = BUTTONS[name];
    const actionDef = ACTIONS[def.action];
    const actionKey = actionDef?.keyTitle ?? actionDef?.key;

    const buttonContainer = document.createElement('div');
    buttonContainer.classList.add('modal__button-container');
    buttonContainer.setAttribute('data-modal-action', def.action);

    const button = document.createElement('button');
    button.classList.add('modal__button', `modal__button--${name}`);
    button.innerHTML = def.title;

    buttonContainer.appendChild(button);

    if (actionKey !== undefined) {
        const keystroke = document.createElement('div');
        keystroke.classList.add('modal__button-keystroke');
        keystroke.innerText = actionKey;
        buttonContainer.appendChild(keystroke);
    }

    return buttonContainer;
}

/**
 * Setups the given action (keystroke).
 *
 * @param {string}        action  - Name of the action
 * @param {{key: string}} options
 */
function setupAction(action, options) {
    ACTIONS[action] = options;
}

/**
 * Adds a button definition.
 *
 * @param {string} name
 * @param {{
 *            title: string,
 *            [action]: string
 *        }} definition
 */
function setupButton(name, definition) {
    if (definition.action === undefined) {
        definition.action = name;
    }
    BUTTONS[name] = definition;
}

/**
 * Setups both an action and a button, with the same name.
 *
 * @param {string} name
 * @param {object} actionDefinition
 * @param {object} buttonDefinition
 */
function setupActionAndButton(name, actionDefinition, buttonDefinition) {
    setupAction(name, actionDefinition);
    setupButton(name, buttonDefinition);
}

/**
 * Shows a new modal.
 *
 * @param {string} contentTemplateId
 * @param {object} [actions] Each key must be a key from the `BUTTONS` dict
 */
function show(contentTemplateId, actions) {
    const modal = new Modal(contentTemplateId, actions);
    modal.open();

    return modal;
}

module.exports = {
    setupButton,
    setupAction,
    setupActionAndButton,
    show,
    Modal,
}
