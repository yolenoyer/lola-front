const routes = require('../routes.json');
const Routing = require('../../../vendor/friendsofsymfony/jsrouting-bundle/Resources/public/js/router.min.js');

Routing.setRoutingData(routes);

/**
 * Generates an url to the given route.
 * See https://symfony.com/doc/master/bundles/FOSJsRoutingBundle/usage.html for usage.
 */
function route(...args) {
    return Routing.generate(...args);
}

module.exports = {
    route,
};
