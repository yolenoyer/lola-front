const { escapeHTML } = require('../util');
const regexp = require('../include/regexp');
const { regexf, checkLookBehindSupport } = regexp;
const wordList = regexp.fromWordList;
const { Highlighter } = require('./highlighter');

const SUPPORTS_LOOKBEHIND = checkLookBehindSupport();

const KEYWORDS = [
    'fn',
    'let',
    'if',       'else',
    'loop',
    'while',
    'for',      'in',
    'return',   'break',    'leave',    'continue',
    'throw',    'try',      'catch',
    'as',
];

const TYPES = [ 'number', 'int', 'string', 'boolean', 'function', 'void', 'array', 'dict' ];

const INT = /\d[_0123456789]*/;
const NUMBER = regexf(/\b%1(?:\.(?:%2)?)?…?/, INT, INT);

const LITERALS = [
    // Literal booleans:
    wordList(['true', 'false']),
];

const STRING = /"(?:[^"\\]|\\.)*"/;

const ID_FIRST_CHARS = [
    'a-zA-Z',
    '_àâäéèêëîïôöùûüŷÿçαβδγοΑΒΔΓΟΩ',
].join('');
const ID_CHARS = ID_FIRST_CHARS + '0-9';

const IDENTIFIER = regexf(/\b[%1][%2]*/, ID_FIRST_CHARS, ID_CHARS);

const FUNCTION = regexf(/%1(?=\s*\()/, IDENTIFIER);

let NUMBER_EXPONENT;
let UNIT;
let QUANTITY;
if (SUPPORTS_LOOKBEHIND) {
    const QUANTITY_FROM_NUMBER = /(?<=\bnumber\s*\()[^)]+/;
    const QUANTITY_SHORTCUT = /(?<=:\s*{\s*)[^}]+/;
    QUANTITY = regexf(/%1|%2/, QUANTITY_FROM_NUMBER, QUANTITY_SHORTCUT);

    const NUMBER_EXPONENT_ALONE = regexf(/[eE]-?%1/, INT);
    const NUMBER_WITH_EXPONENT = regexf(/%1(?:%2)?/, NUMBER, NUMBER_EXPONENT_ALONE);
    NUMBER_EXPONENT = regexf(/(?<=%1)%2/, NUMBER, NUMBER_EXPONENT_ALONE);

    const UNIT_SPECIAL_CHARS = "μ°Ω$€";
    const UNIT_CHAR = regexf(/[A-Za-z%1]/, UNIT_SPECIAL_CHARS);
    const UNIT_EXPONENT = /-?\d+/;
    const EXP_UNIT = regexf(/%1+(?:%2)?/, UNIT_CHAR, UNIT_EXPONENT);
    const EXTRA_EXP_UNIT = regexf(/[./]%1/, EXP_UNIT);
    const UNIT_ALONE = regexf(/%1(?:%2)*/, EXP_UNIT, EXTRA_EXP_UNIT);
    const UNIT_CONVERSION_OPERATOR = /~!?/;
    UNIT = regexf(/(?<=(?:%1\s*|%2))%3/, NUMBER_WITH_EXPONENT, UNIT_CONVERSION_OPERATOR, UNIT_ALONE);
}

const innerStringHighlighter = new Highlighter(highlightToken, {
    'string-escape': /\\./
});

const highlighter = new Highlighter(highlightToken, {
    keyword: wordList(KEYWORDS),
    type: wordList(TYPES),
    number: NUMBER,
    literal: regexp.any(LITERALS),
    string: [STRING, innerStringHighlighter],
    function: FUNCTION,
    comment: /\/\/.*/,
});

if (SUPPORTS_LOOKBEHIND) {
    highlighter.add({
        quantity: QUANTITY,
        exponent: NUMBER_EXPONENT,
        unit: UNIT,
    });
}

/**
 * Callback used with CodeJar.
 *
 * @param {HTMLElement} editor
 */
function highlight(editor) {
    let code = escapeHTML(editor.textContent);
    editor.innerHTML = highlighter.highlight(code);
}

/**
 * @param {string} token
 * @param {TokenDef} tokenDef
 *
 * @return {string}
 */
function highlightToken(token, tokenDef) {
    return `<span class="code-token--${tokenDef.name}">${token}</span>`;
}

module.exports = {
    highlight,
    highlighter,
}
