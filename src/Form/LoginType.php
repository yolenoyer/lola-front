<?php

namespace App\Form;

use App\Entity\LoginInfo;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class LoginType
 *
 * @category lola-front
 * @package  lola-front
 * @author   Johann David <johann.david.dev@protonmail.com>
 */
class LoginType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('username', null, [ 'label' => "Utilisateur" ])
            ->add('password', PasswordType::class, [ 'label' => "Mot de passe" ])
            ->add('submit', SubmitType::class, [ 'label' => "Connexion" ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => LoginInfo::class,
            'csrf_protection' => true,
            'csrf_token_id' => 'login_item',
        ]);
    }
}
