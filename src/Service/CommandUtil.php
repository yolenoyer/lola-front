<?php

namespace App\Service;

use Exception;
use Symfony\Bundle\FrameworkBundle\Console\Application;
use Symfony\Component\Console\Input\ArrayInput;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\HttpKernel\KernelInterface;

/**
 * Class CommandUtil
 *
 * @category lola-front
 * @package  lola-front
 * @author   Johann David <johann.david.dev@protonmail.com>
 */
class CommandUtil
{
    /**
     * @var KernelInterface
     */
    protected KernelInterface $kernel;

    /**
     * CommandUtil constructor.
     *
     * @param KernelInterface $kernel
     */
    public function __construct(KernelInterface $kernel)
    {
        $this->kernel = $kernel;
    }

    /**
     * @param string               $commandName
     * @param array                $args
     * @param OutputInterface|null $output
     *
     * @throws Exception
     */
    public function runCommand(OutputInterface $output, string $commandName, array $args = [])
    {
        $application = new Application($this->kernel);
        $application->setAutoExit(false);

        $input = new ArrayInput(array_merge(
            [ 'command' => $commandName ],
            $args
        ));

        $application->run($input, $output);
    }
}
