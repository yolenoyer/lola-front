<?php

declare(strict_types=1);

namespace App\Security;

use App\Exception\LolaApiException;
use App\Repository\Api\LolaApi;
use Symfony\Component\Security\Core\Authentication\AuthenticationManagerInterface;
use Symfony\Component\Security\Core\Authentication\Token\NullToken;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Symfony\Component\Security\Core\Security;


/**
 * Class SecurityHandler
 *
 * @category lola-front
 * @package  lola-front
 * @author   Johann David <johann.david.dev@protonmail.com>
 */
class SecurityHandler
{
    protected AuthenticationManagerInterface $authenticationManager;
    protected TokenStorageInterface $tokenStorage;
    protected Security $security;
    protected LolaApi $lolaApi;

    /**
     * @param AuthenticationManagerInterface $authenticationManager
     * @param TokenStorageInterface          $tokenStorage
     * @param Security                       $security
     * @param LolaApi                        $lolaApi
     */
    public function __construct(
        AuthenticationManagerInterface $authenticationManager,
        TokenStorageInterface $tokenStorage,
        Security $security,
        LolaApi $lolaApi
    )
    {
        $this->authenticationManager = $authenticationManager;
        $this->tokenStorage = $tokenStorage;
        $this->security = $security;
        $this->lolaApi = $lolaApi;
    }

    /**
     * @return User|null
     */
    public function getUser(): ?User {
        /** @var User $user */
        /** @noinspection PhpUnnecessaryLocalVariableInspection */
        $user = $this->security->getUser();
        return $user;
    }

    /**
     * Checks if an admin user is connected.
     *
     * @return bool
     */
    public function isAdmin(): bool
    {
        $user = $this->getUser();
        return $user && $user->isAdmin();
    }

    /**
     * Connects the user.
     *
     * @param string $username
     * @param string $password
     *
     * @throws ConnectionException
     * @throws LolaApiException
     */
    public function connect(string $username, string $password)
    {
        $user = $this->newUserFromApi($username, $password);
        $token = new LolaServerUserToken($user, 'main', $user->getRoles());
        $authenticatedToken = $this->authenticationManager->authenticate($token);
        $this->tokenStorage->setToken($authenticatedToken);
    }

    /**
     * Disconnects the user.
     */
    public function disconnect()
    {
        $this->tokenStorage->setToken(new NullToken());
    }

    /**
     * @param string $username
     * @param string $password
     *
     * @return User
     *
     * @throws ConnectionException|LolaApiException
     */
    function newUserFromApi(string $username, string $password): User
    {
        $apiResponse = $this->lolaApi->adminLogin($username, $password);

        if ($apiResponse['success']) {
            return (new User())
                ->setRoles(['ROLE_ADMIN', 'ROLE_USER'])
                ->setIsAdmin(true)
                ->setUsername($username)
                ->setJwt($apiResponse['payload']['token']);
        } else {
            throw new ConnectionException($apiResponse['error']);
        }
    }
}
