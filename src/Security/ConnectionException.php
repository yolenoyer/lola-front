<?php

declare(strict_types=1);

namespace App\Security;

use Exception;

/**
 * Class ConnectionException
 *
 * @category lola-front
 * @package  lola-front
 * @author   Johann David <johann.david.dev@protonmail.com>
 */
class ConnectionException extends Exception
{
}
